var os = require('os');
var path = require('path');
var webpack = require('webpack');
var _ = require('lodash');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var StatsPlugin = require('./statsPlugin');

var prod = process.env.NODE_ENV === 'production';

var jsName = '[name].[hash].js';
var staticName = '[sha512:hash:base36:7].[ext]';

var config = {
  entry: {
    project: ['./src/main']
  },
  output: {
    path: path.join(__dirname, 'build'),
    filename: jsName,
    publicPath: prod ? '/build/' : 'http://127.0.0.1:3001/build/'
  },
  resolve: {
    alias: {
      components: path.join(__dirname, 'src', 'components'),
      ui: path.join(__dirname, 'src', 'ui'),
      mixins: path.join(__dirname, 'src', 'mixins'),
      stores: path.join(__dirname, 'src', 'stores'),
      actions: path.join(__dirname, 'src', 'actions'),
      utils: path.join(__dirname, 'src', 'utils'),
      test: path.join(__dirname, 'test')
    }
  },
  module: {
    loaders: [
      {
        test: /\.yml$/,
        loader: 'json!yaml'
      },
      {
        test: /\.json$/,
        loader: 'json'
      },
      {
        test: /\.styl$/,
        loader: 'style!css!autoprefixer!stylus'
      },
      {
        test: /\.css$/,
        loader: 'style!css'
      },
      {
        test: /\.(ttf|woff|woff2|eot|svg|gif|png|ico)(\?.+)?$/,
        loader: 'file-loader?name=' + staticName
      },
      {
        test: /\.js$/,
        loaders: ['babel-loader?experimental&optional=runtime'],
        exclude: /node_modules/,
      }
    ]
  },
  stylus: {
    use: [require('jeet')()]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || "development")
    }),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.DedupePlugin(),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/),
    new webpack.NoErrorsPlugin(),
    new StatsPlugin()
  ]
};

if (prod) {

  config.devtool = 'sourcemap';

  config.plugins.unshift(
    new webpack.optimize.UglifyJsPlugin({comments: /a^/, compress: {warnings: false}})
  );

} else {

  config.devtool = 'eval';

  _.find(config.module.loaders, function(loader) {
    return loader.test.source === /\.js$/.source;
  }).loaders.unshift('react-hot');

  config.entry.project.unshift(
    'webpack-dev-server/client?http://' + os.hostname() + ':3001',
    'webpack/hot/dev-server'
  );

  config.plugins.push(new webpack.HotModuleReplacementPlugin());
}

module.exports = config;
