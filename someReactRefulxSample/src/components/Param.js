var React = require('react');
var cx = require('react/lib/cx');
var PropTypes = require('react/lib/ReactPropTypes');
var Colorized = require('mixins/Colorized');
var Pure = require('mixins/Pure');
var PrizmActions = require('actions/PrizmActions');
var Immutable = require('immutable');
var Numeral = require('numeral');
var sort = require('utils/sort');
var tweenState = require('react-tween-state');
var ItemTypes = require('components/itemTypes');

import {TriangleDown, TriangleUp} from './Arrows';
import {DragDropMixin} from 'react-dnd';

var BAR_WIDTH = 92;
var BAR_HEIGHT = 8;
var PARAM_HEIGHT = 24;
var ARROW_WIDTH = 6;

var Exceeds = Immutable.Record({
  visits: false,
  affinity: false
});

var AffinityGraph = React.createClass({

  mixins: [Pure, Colorized],

  propTypes: {
    value: PropTypes.number.isRequired,
    exceeds: PropTypes.bool.isRequired,
    limit: PropTypes.number.isRequired,
    belowOne: PropTypes.bool.isRequired,
    equalsOne: PropTypes.bool.isRequired,
    displayValue: PropTypes.number.isRequired,
    displayValueMax: PropTypes.number.isRequired,
  },

  valueWidth() {
    if (this.props.belowOne) {
      return Math.abs(Math.floor(this.props.value * (BAR_WIDTH / 2)));
    } else if (this.props.exceeds) {
      //return Math.abs(Math.floor((this.props.value / this.props.limit) * (BAR_WIDTH / 2)));
      return Math.abs(Math.floor((BAR_WIDTH / 2)));
    } else {
      const displayWidthProportion =
        (this.props.displayValue - 100) /
        (this.props.displayValueMax - 100);
      return Math.abs(Math.floor((Math.min(displayWidthProportion, 1)) * (BAR_WIDTH / 2)));
    }
  },

  barStyle() {
    if (this.props.equalsOne) {
      return {};
    }
    var direction = 'right';
    var position = `${Math.round(BAR_WIDTH / 2)}px, 0px`;
    var valueWidth = this.valueWidth();
    if (this.props.belowOne) {
      direction = 'left';
      position = `${Math.round(BAR_WIDTH / 2) - valueWidth}px, 0px`;
    }
    return {
      backgroundImage: `linear-gradient(to ${direction}, ${this.getStartColor()} 0px, ${this.getEndColor()} ${Math.round(BAR_WIDTH / 2)}px)`,
      backgroundSize: `${valueWidth}px 100%`,
      backgroundPosition: position
    };
  },

  arrowStyle() {
    return {
      marginLeft: `${Math.round(this.valueWidth() + BAR_WIDTH / 2)}px`,
      borderColor: `transparent transparent transparent ${this.getEndColor()}`,
      borderWidth: `${BAR_HEIGHT / 2}px 0 ${BAR_HEIGHT / 2}px ${ARROW_WIDTH}px`
    }
  },

  render() {
    return (
      <div className='AffinityGraph'>
        <div className='AffinityGraph-bar' style={this.barStyle()} />
        {this.props.exceeds &&
        <div className='AffinityGraph-arrow' style={this.arrowStyle()}>
          <div className='AffinityGraph-plus'>+</div>
        </div>
        }
        <div className='AffinityGraph-zero' />
      </div>
    );
  }

});


var VisitsGraph = React.createClass({

  mixins: [Pure, Colorized],

  propTypes: {
    value: PropTypes.number.isRequired,
    exceeds: PropTypes.bool.isRequired,
    limit: PropTypes.number.isRequired
  },

  valueWidth() {
    if(this.props.exceeds){
      return Math.abs(Math.floor(BAR_WIDTH));
    }
    return Math.abs(Math.floor((this.props.value / this.props.limit) * BAR_WIDTH));
  },

  barStyle() {
    var valueWidth = this.valueWidth();
    return {
      backgroundImage: `linear-gradient(to right, ${this.getStartColor()} 0px, ${this.getEndColor()} ${Math.round(BAR_WIDTH / 2)}px)`,
      backgroundSize: `${valueWidth}px 100%`
    };
  },

  arrowStyle() {
    return {
      marginLeft: `${this.valueWidth()}px`,
      borderColor: `transparent transparent transparent ${this.getEndColor()}`,
      borderWidth: `${BAR_HEIGHT / 2}px 0 ${BAR_HEIGHT / 2}px ${ARROW_WIDTH}px`
    }
  },

  render() {
    return (
      <div className='VisitsGraph'>
        <div className='VisitsGraph-bar' style={this.barStyle()} />
        {this.props.exceeds &&
        <div className='VisitsGraph-arrow' style={this.arrowStyle()}>
          <div className='VisitsGraph-plus'>+</div>
        </div>
        }
      </div>
    );
  }

});


var EmptyGraph = React.createClass({
  mixins: [Pure],

  onClick() {
    if (this.props.node.children.size) {
      PrizmActions.toggleParam(this.props.node.id);
    }
  },

  render() {
    var zero = null;
    var expanding = null;
    if (this.props.node.children.size) {
      expanding = this.props.node.expanded
        ? <TriangleUp className='EmptyGraph-triangle' />
        : <TriangleDown className='EmptyGraph-triangle' />;
    } else if (this.props.reportMode === 'affinity') {
      zero = <div className='EmptyGraph-zero' />;
    }
    return (
      <div className={cx('EmptyGraph', 'withChildren')} onClick={this.onClick}>
        {expanding}
        <div className='EmptyGraph-bar' />
        {zero}
      </div>
    );
  }
});

const dragSource = {
  beginDrag(component) {
    return {
      item: {
        name: component.props.node.name
      }
    };
  },

  endDrag(component, effect) {
    if (effect) {
      var dropParams = component.props.handleDropZone();
      PrizmActions.setDropContent(
        component.props.node.id,
        component.props.node.toJS(),
        dropParams.dropZone,
        dropParams.dropBlockIndex
      )
    }
  }
};

var Param = React.createClass({
  mixins: [tweenState.Mixin],

  getDefaultProps() {
    return {
      level: 0
    };
  },

  getInitialState() {
    return this.getState(this.props);
  },

  getState(props) {
    const exceeds = new Exceeds({
      visits:   props.node.visits ? props.limits.visits       < props.node.visits        : false,
      affinity: props.node.affinityIndex ? props.limits.affinityIndex < props.node.affinityIndex : false
    });
    return {
      valueAffinity: exceeds.affinity
        ? props.limits.affinity
        : props.node.affinityIndex >= 1 ? props.node.affinity : (1 - props.node.affinityIndex),
      aff: props.node.affinity,
      affIndex: props.node.affinityIndex,
      valueVisits: exceeds.visits
        ? props.limits.visits
        : props.node.visits,
      belowOne: props.node.affinityIndex < 1,
      equalsOne: props.node.affinityIndex == 1,
      displayAffinity: props.node.affinityIndex * 100,
      displayVisits: props.node.visits,
      exceeds: exceeds
    }
  },

  componentWillReceiveProps(props) {
    var prevReportMode = this.props.reportMode;
    this.setState(this.getState(props), this.updateTweenValues.bind(this, prevReportMode)) ;
  },

  componentDidMount() {
    this.setState(this.getState(this.props), this.updateTweenValuesSubNodes.bind(this, this.props.reportMode)) ;
  },

  updateTweenValues(prevReportMode) {
    if (prevReportMode !== this.props.reportMode) {
      if (prevReportMode === 'affinity') {
        this.addTweenings(500, 'Affinity', 'Visits');
      }
      if (prevReportMode === 'visits') {
        this.addTweenings(500, 'Visits', 'Affinity');
      }
    } else {
      this.updateTweenValuesSubNodes(prevReportMode);
    }
  },

  updateTweenValuesSubNodes(reportMode) {
    if (reportMode === 'affinity') {
      this.setTweeningsNoDelay('Visits');
    }
    if (reportMode === 'visits') {
      this.setTweeningsNoDelay('Affinity');
    }
  },

  addTweenings(t, from, to) {
    this.tweenState('value' + from, {
      easing: tweenState.easingTypes.easeInQuad,
      duration: t,
      endValue: 0
    });
    this.tweenState('display' + from, {
      easing: tweenState.easingTypes.easeInQuad,
      duration: t,
      endValue: 0
    });
    this.tweenState('value' + to, {
      easing: tweenState.easingTypes.easeInQuad,
      delay: t,
      duration: t,
      beginValue: 0,
      endValue: this.state['value' + to]
    });
    this.tweenState('display' + to, {
      easing: tweenState.easingTypes.easeInQuad,
      delay: t,
      duration: t,
      beginValue: 0,
      endValue: this.state['display' + to]
    });
  },

  setTweeningsNoDelay(from) {
    this.tweenState('value' + from, {
      easing: tweenState.easingTypes.easeInQuad,
      duration: 0,
      endValue: 0
    });
    this.tweenState('display' + from, {
      easing: tweenState.easingTypes.easeInQuad,
      duration: 0,
      endValue: 0
    });
  },

  toggleStyle() {
    return {
      left: this.props.level * 10 - 16
    };
  },

  onClick() {
    if (this.props.node.children.size) {
      PrizmActions.toggleParam(this.props.node.id);
    }
  },

  fullValueFormatter(val) {
    return Number(val) ? outputFormatter(val) : val;

    function outputFormatter (val) {
      return val.replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1\u2009');
    }
  },

  valueFormatter(val) {
    var self = this;
    return Number(val) ? numberToAbbr(val) : val;

    function numberToAbbr (num, decPlaces = 3) {
      var numOutput = num;
      var dec = Math.pow(10, decPlaces);
      var abbr = ['k', 'M'];
      var abbrLength = abbr.length;
      var size;
      var cutOffRange;

      if (numOutput >= 10000) {
        while (abbrLength >= 0) {
          abbrLength -= 1;
          size = Math.pow(10, (abbrLength + 1) * 3);

          if (size <= numOutput) {
            let preFormatted = Math.round(numOutput * dec / size) / dec + '';
            cutOffRange = Number(preFormatted[4]) === 0 ? 4 : 5;
            numOutput = preFormatted.substr(0, cutOffRange);
            if (numOutput[numOutput.length - 1] === '.') {
              numOutput = numOutput.substring(0, numOutput.length - 1);
            }
            numOutput += abbr[abbrLength];

            break;
          }
        }
      } else {
        numOutput = self.fullValueFormatter(numOutput);
      }

      return numOutput.replace('.', ',');
    }
  },

  render() {
    var childNodes = this.props.node.children;
    if (!this.props.showEmpty) {
      childNodes = childNodes.filterNot(x => x.empty);
    }

    var toggle = null;

    if (childNodes.size) {
      var toggleClass = `Param-toggle fa fa-${this.props.node.expanded ? 'minus' : 'plus'}-square-o`;
      toggle = (
        <div onClick={this.onClick} style={this.toggleStyle()}
             className={toggleClass} />
      );
    }

    var children = null;
    if (this.props.node.expanded) {
      var sortedChildren = childNodes.sort(sort.getComparator(this.props.sortMode, this.props.sortDirection));

      var params = sortedChildren.map(
        (child) => <Param level={this.props.level + 1} node={child} qwe={child.toJS()} key={child.id}
                          limits={this.props.limits}
                          reportMode={this.props.reportMode}
                          showEmpty={this.props.showEmpty}
                          sortMode={this.props.sortMode}
                          sortDirection={this.props.sortDirection}
                          handleDropZone={this.props.handleDropZone}/>);

      children = (
        <div className='Param-children'>
          {params.toArray()}
        </div>
      );
    }

    var displayValue;
    if (this.props.node.affinity === null) {
      displayValue = this.props.node.children.size ? '...' : 'n/a';
    } else if (this.getTweeningValue('valueAffinity') !== 0) {
      displayValue = Numeral(this.getTweeningValue('displayAffinity')).format('0');
    } else {
      displayValue = Numeral(this.getTweeningValue('displayVisits')).format('0');
    }

    var graph;
    if (this.props.node.affinity === null) {
      graph = <EmptyGraph node={this.props.node} reportMode={this.props.reportMode} />;
    } else {
      if (this.getTweeningValue('valueAffinity') !== 0) {
        graph = <AffinityGraph value={this.getTweeningValue('valueAffinity')}
                               exceeds={this.state.exceeds.affinity}
                               limit={this.props.limits.affinity}
                               belowOne={this.state.belowOne}
                               equalsOne={this.state.equalsOne}
                               displayValue={parseInt(displayValue, 10)}
                               displayValueMax={this.props.limits.affinityIndex * 100}
          />;
      } else {
        graph = <VisitsGraph value={this.getTweeningValue('valueVisits')}
                             exceeds={this.state.exceeds.visits}
                             limit={this.props.limits.visits}
          />;
      }
    }

    var className = cx({
      Param,
      expanded: this.props.node.expanded,
      empty: this.props.node.empty
    });

    return (
      <div className={className} >
        <div className='Param-row'>
          {toggle}
          <ParamName {...this.props} clickHandler={this.onClick}/>
          {graph}
          <div className='Param-value Param-value--full'>{this.fullValueFormatter(displayValue)}</div>
          <div className='Param-value Param-value--short'>{this.valueFormatter(displayValue)}</div>
        </div>
        {children}
      </div>
    );
  }

});

var ParamName = React.createClass({


  mixins: [DragDropMixin],

  statics: {
    configureDragDrop(register) {
      register(ItemTypes.ITEM, { dragSource });
    }
  },

  nameStyle() {
    return {
      cursor: this.props.node.children.size ? 'pointer' : 'default',
      left: this.props.level * 10,
      maxWidth: 240 - this.props.level * 10,
    };
  },

  render(){
    return (
      <div className='Param-name'
           style={this.nameStyle()}
           onClick={this.props.clickHandler}
                   {...(this.props.node.isSegment && this.dragSourceFor(ItemTypes.ITEM) || {})}>
          {this.props.node.name}
      </div>
    );
  }
});

module.exports = Param;
