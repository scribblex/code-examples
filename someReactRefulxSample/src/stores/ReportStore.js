var axios = require('axios');
var Reflux = require('reflux');
var invariant = require('react/lib/invariant');
var _ = require('lodash');
var moment = require('moment');
var Immutable = require('immutable');
let debug = require('debug');

var ReportActions = require('../actions/ReportActions');
var ProjectActions = require('../actions/ProjectActions');
var AuthActions = require('../actions/AuthActions');
var ImmutableReport = require('./ImmutableReport');

require('../vendor/Blob.js');
var FileSaver = require('../vendor/FileSaver.js');

debug = debug('project:ReportStore');


var ReportStore = Reflux.createStore({

  init: function() {
    this.now = moment();
    this.loading = false;
    this.loadingDomain = null;
    this.statMode = 'single';
    this.domains = [];
    this.reportMode = 'affinity';
    this.showEmpty = false;
    this.sortMode = 'power';
    this.domainList = null;
    this.domainListExpanded = false;
    this.domainDates = null;
    this.sortDirection = -1;
    this.customSegmentsId = 2961;
    this.segments = null;
    this.firstName = null;
    this.lastName = null;
    this.exportInProgress = false;
    this.dateFormatCurrent = null;
    this.snapShotDate      = null;
    this.dateFormat        = {d: "YYYY-MM-DD", w:"YYYY-WW", m: "YYYY-MM"};
    this.constructorIsOpened = false;

    this.listenTo(ReportActions.load, this.load);
    this.listenTo(ReportActions.loadMultiple, this.loadMultiple);
    this.listenTo(ProjectActions.toggleParam, this.toggleParam);
    this.listenTo(ProjectActions.toggleDomainListExpanded, this.toggleDomainListExpanded);
    this.listenTo(ProjectActions.setReportMode, this.setReportMode);
    this.listenTo(ProjectActions.setShowEmpty, this.setShowEmpty);
    this.listenTo(ProjectActions.setSortMode, this.setSortMode);
    this.listenTo(ProjectActions.setSortDirection, this.setSortDirection);
    this.listenTo(ProjectActions.setDropContent, this.onSetDropContent);
    this.listenTo(ProjectActions.clearDropBlocks, this.onClearDropBlocks);
    this.listenTo(ProjectActions.removeDropContent, this.onRemoveDropContent);
    this.listenTo(ProjectActions.removeDropBlock, this.onRemoveDropBlock);
    this.listenTo(ProjectActions.cloneDropBlock, this.onCloneDropBlock);
    this.listenTo(ProjectActions.addDropBlock, this.onAddDropBlock);
    this.listenTo(ProjectActions.toggleConstructor, this.toggleConstructor);
    this.listenTo(AuthActions.setDomainList, this.setDomainList);
    this.listenTo(AuthActions.setSegments, this.setSegments);
    this.listenTo(AuthActions.setSegmentsDictionary, this.setSegmentsDictionary);
    this.listenTo(AuthActions.setUser, this.setUser);
    this.listenTo(ReportActions.setCalendarDate, this.setCalendarDate);
    this.listenTo(ReportActions.setCalendarMonth, this.setCalendarMonth);
    this.listenTo(ReportActions.exportToXls, this.exportToXls);
    this.listenTo(ReportActions.exportCancel, this.exportCancel);
    this.listenTo(ReportActions.setStatMode, this.setStatMode);
    this.listenTo(ReportActions.toggleDomain, this.toggleDomain);
    this.listenTo(ReportActions.loadSnapshot, this.loadSnapshot);
    this.listenTo(ReportActions.setCalendarSnapshot, this.setCalendarSnapshot);
    this.listenTo(ReportActions.setSnapshotDate, this.setSnapshotDate);
    this.listenTo(ReportActions.setSnapshotDay, this.setSnapshotDay);
    this.listenTo(ReportActions.setSnapshotWeek, this.setSnapshotWeek);
    this.listenTo(ReportActions.setSnapshotMonth, this.setSnapshotMonth);
    this.report = {
      nodes: Immutable.List()
    };

    this._reportCache = {};

    this.dropContent = Immutable.List.of(
      this.createDropContent()
    );

    const now = moment();
    this.calendarMonth = moment(now).startOf('month');
    this.calendarDate = moment(now).startOf('day');
  },

  createDropContent () {
    return Immutable.Map({
      include: Immutable.Map(),
      exclude: Immutable.Map()
    });
  },

  onSetDropContent(id, value, dropState, key) {
    if (this.dropContent.has(key)) {
      var list = this.dropContent.get(key).get(dropState);
      this._compareDropContent(dropState, key, id);
      if (!list.get(id)) {
        this._setDropContent(id, value, dropState, key);
      }
    } else {
      this.dropContent = this.dropContent.set(key, this.createDropContent());
      this._setDropContent(id, value, dropState, key);
    }
    this.triggerAsync();
  },

  onRemoveDropContent (dropState, dropBlockIndex, id) {
    this._removeDropContent(dropState, dropBlockIndex, id);
    this.triggerAsync();
  },

  _setDropContent (id, value, dropState, key) {
    this.dropContent = this.dropContent.set(
      key,
      this.dropContent.get(key).set(
        dropState,
        this.dropContent.get(key).get(dropState).set(id, value)
      )
    );
  },

  _removeDropContent (dropState, dropBlockIndex, id) {
    this.dropContent = this.dropContent.set(
      dropBlockIndex,
      this.dropContent.get(dropBlockIndex).set(
        dropState,
        this.dropContent.get(dropBlockIndex).get(dropState).delete(id)
      )
    );
  },

  _compareDropContent(dropZone, dropBlockIndex, id) {
    var include = 'include';
    var exclude = 'exclude';
    var alterDropZone = (dropZone === include) ? exclude :
      (dropZone === exclude) ? include : undefined;

    if (this.dropContent.get(dropBlockIndex).get(alterDropZone).get(id)) {
      this._removeDropContent(alterDropZone, dropBlockIndex, id);
    }
  },

  onClearDropBlocks () {
    this.dropContent = Immutable.List.of(
      this.createDropContent()
    );
    this.triggerAsync();
  },

  onRemoveDropBlock (dropBlockIndex) {
    var size = this.dropContent.size;
    this.dropContent = this.dropContent.delete(dropBlockIndex);
    if(size === 1){
      this.dropContent = this.dropContent.push(this.createDropContent());
    }
    this.triggerAsync();
  },

  onCloneDropBlock (dropBlockIndex) {
    this.dropContent = this.dropContent.push(
      this.dropContent.get(dropBlockIndex)
    );
    this.triggerAsync();
  },

  onAddDropBlock () {
    this.dropContent = this.dropContent.push(this.createDropContent());
    this.triggerAsync();
  },

  toggleConstructor () {
    this.constructorIsOpened = !this.constructorIsOpened;
    this.triggerAsync();
  },

  setCalendarMonth(month) {
    this.calendarMonth = month;
    this.triggerAsync()
  },

  setCalendarDate(day) {
    if (!this.loading) {
      ReportActions.load({
        day,
        domain: this.domains[0]
      });
    }
  },

  setCalendarSnapshot() {
    if (!this.loading) {
      ReportActions.loadSnapshot({
        date: this.snapShotDate,
        domain: this.domains[0],
        format: this.dateFormatCurrent
      });
    }
  },

  setSnapshotDate(date, format) {
    switch (format) {
      case 'd':
        this.setSnapshotDay(date);
        break;
      case 'w':
        this.setSnapshotWeek(date);
        break;
      case 'm':
        this.setSnapshotMonth(date);
        break;
    }
    this.triggerAsync();
  },

  setSnapshotDay(date) {
    const format           = 'd';
    this.snapShotDate      = moment(date, this.dateFormat[format]);
    this.dateFormatCurrent = format;
    this.triggerAsync();
  },

  setSnapshotWeek(date) {
    const format           = 'w';
    this.snapShotDate      = moment(date, this.dateFormat[format]);
    this.dateFormatCurrent = format;
    this.triggerAsync();
  },

  setSnapshotMonth(date) {
    const format           = 'm';
    this.snapShotDate      = moment(date, this.dateFormat[format]);
    this.dateFormatCurrent = format;
    this.triggerAsync();
  },

  setUser({firstName, lastName, email}) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.triggerAsync();
  },

  setSegments(segments) {
    this.segments = segments;
    this.triggerAsync();
  },

  setSegmentsDictionary(data) {
    this.dmpSegments = _.reduce(data.dmpSegments, (dict, item)=>{dict[item.id]=item; return dict;}, {});
    this.parentDmpSegments = _.reduce(data.parentDmpSegments, (dict, item)=>{dict[item.id]=item; return dict;}, {});
    this.triggerAsync();
  },

  setDomainList(domainList) {
    if (this.loading) {
      return;
    }
    this.domainDates = domainList;
    this.domainList = Object.keys(domainList);
    const selectIndex = _.findIndex(this.domainList, (domain) => domainList[domain].length);
    if (selectIndex > -1) {
      var domain = this.domainList[selectIndex];
      var lastDomain = localStorage.lastDomain;
      if (lastDomain && this.domainList.indexOf(lastDomain) !== -1 && domainList[lastDomain] && domainList[lastDomain].length) {
        domain = lastDomain;
      }
      ReportActions.load({
        domain,
        day: domainList[domain][0]
      });
    } else {
      this.triggerAsync();
    }
  },

  setSortDirection(direction) {
    if (direction !== this.sortDirection) {
      this.sortDirection = direction;
      this.triggerAsync();
    }
  },

  setSortMode(mode) {
    if (mode !== this.sortMode) {
      this.sortMode = mode;
      this.triggerAsync();
    }
  },

  setShowEmpty(showEmpty) {
    if (showEmpty !== this.showEmpty) {
      this.showEmpty = showEmpty;
      this.trigger();
      requestAnimationFrame(function() {
        window.scrollTo(0, 0);
      });
    }
  },

  setReportMode: function(newMode) {
    if (newMode !== this.reportMode) {
      this.reportMode = newMode;
      this.triggerAsync();
    }
  },

  toggleParam: function(id) {
    var childPath = null;

    function iterChildren(node, path) {
      if (node.id === id) {
        childPath = path;
        return false;
      }
      node.children.forEach(function(child, index) {
        if (childPath) {
          return false;
        }
        iterChildren(child, path.push('children').push(index));
      });
    }

    this.report.nodes.forEach(function(node, index) {
      if (childPath) {
        return false;
      }
      iterChildren(node, Immutable.List([index]));
    });

    this.report = new ImmutableReport.Report({
      limits: this.report.limits,
      nodes: this.report.nodes.updateIn(childPath.push('expanded').toArray(), (x) => !x)
    });

    this.triggerAsync();
  },

  toggleDomainListExpanded: function() {
    this.domainListExpanded = !this.domainListExpanded;

    this.triggerAsync();
  },

  load: function({domain, day}) {
    invariant(this.loading === false, 'ReportStore is already in a loading state');

    if (!day) {
      const dates = this.domainDates[domain];
      day = dates[dates.length - 1];
    }

    if (typeof day === 'string') {
      day = moment(day, 'DD.MM.YY');
    } else {
      day = moment(day).startOf('day');
    }

    const cacheKey = `${domain}_${day.format('DD.MM.YY')}`;

    if (day.isSame(moment(), 'day')) {
      debug('loading live statistics', domain, day.format('DD.MM.YY'));
      this._load(domain, day, true);
    } else {
      debug('loading', domain, day.format('DD.MM.YY'));
      this._load(domain, day);
    }
  },

  loadSnapshot: function({domain, date, format}) {
    invariant(this.loading === false, 'ReportStore is already in a loading state');

    if (!date) {
      debug('loading last snapshot period: ' + format, domain);
      this._loadSnapshot(domain, null, format, true);
    } else {
      const cacheKey = `${domain}_${date.format('YYYY-MM-DD')}`;
      if (this._reportCache[cacheKey]) {
        debug('loading from cache', domain, day.format('DD.MM.YY'));
        this.report        = this._reportCache[cacheKey];
        this.domains       = [domain];
        this.snapShotDate  = date;
        this.loading       = false;
        this.loadingDomain = null;
        this.triggerAsync();
        ReportActions.domainChange(domain);
      } else {
        debug('loading', domain, date, format);
        this._loadSnapshot(domain, date, format);
      }
    }
  },

  _loadSnapshot: function(domain, date, range, live = false) {
    this.loading = true;
    this.loadingDomain = domain;
    const postDate = date.format(this.dateFormat[range]);
    this.triggerAsync();

    axios.post(`/api/affinityPeriodicReport?live=${live}`, {segments: this.segments, domain: domain, date: postDate, range: range})
      .then(function({data}) {
        if (!live) {
          this._storeInCache(domain, data.tree.nodes, data.tree.roots, data.report, data.limits);
        }
        this._update(domain, data.tree.nodes, data.tree.roots, data.report, data.limits);
      }.bind(this))
      .catch(function(err) {
        this._update(domain);
        alert('Ошибка загрузки отчета');
      }.bind(this));
  },

  loadMultiple: function() {
    debug('loading live statistics for multiple domains', this.domains);
    this._load(this.domains, moment(), true);
  },

  exportToXls: function() {
    this.exportInProgress = true;
    this.triggerAsync();
    axios.post('/api/someApiMethod', {
      'domain': this.domains,
      'segments': this.segments
    }).then((response) => {
      if (this.exportInProgress) {
        this.exportInProgress = false;
        this.triggerAsync();
        var blob = new Blob([response.data], {type: 'text/plain;charset=utf-8'});
        FileSaver.saveAs(blob, 'affinity-report.csv');
      }
    });
  },

  exportCancel: function() {
    this.exportInProgress = false;
    this.triggerAsync();
  },

  setStatMode: function(mode) {
    this.statMode = mode;
    if (mode === 'single') {
      this.domains = this.domains.slice(0, 1);
    }
    this.triggerAsync();
  },

  toggleDomain: function({domain}) {
    if (!_.contains(this.domains, domain)) {
      this.domains.push(domain);
    } else if (this.domains.length > 1) {
      _.remove(this.domains, d => d === domain);
    }
    this.domains = this.domains.slice();
    this.triggerAsync();
  },

  _update: function(domain, nodes, roots, report, limits) {
    this.report = ImmutableReport.create({nodes, roots}, report, limits);
    this.domains = _.isArray(domain) ? domain : [domain];
    this.loading = false;
    this.loadingDomain = null;

    this._reportCache[`${this.domains[0]}_${this.calendarDate.format('DD.MM.YY')}`] = this.report;

    this.triggerAsync();
    localStorage.lastDomain = domain;
    ReportActions.domainChange(domain);
  },

  _loadFromCache: function(domain) {
    var data = JSON.parse(localStorage[this._cacheKey(domain)]);
    this._update(domain, data.tree.nodes, data.tree.roots, data.report, data.limits);
  },

  _load: function(domain, day, live = false) {
    this.loading = true;
    this.loadingDomain = domain;
    this.calendarDate = moment(day);
    this.calendarMonth = moment(day).startOf('month');
    this.triggerAsync();

    var reqData = {segments: this.segments, domain: domain, range: 'd', date: day.format('YYYY-MM-DD')};
    axios.post(`/api/affinityReport?live=${live}`, reqData)
      .then(function({data}) {
        if (!live) {
          this._storeInCache(domain, data.tree.nodes, data.tree.roots, data.report, data.limits);
        }
        this._update(domain, data.tree.nodes, data.tree.roots, data.report, data.limits);
      }.bind(this))
      .catch(function(err) {
        this._update(domain);
      }.bind(this));
  },

  _hasCached: function(domain) {
    return !! localStorage[this._cacheKey(this.domains[0])];
  },

  _today: function() {
    return this.now.format('DDMMYY');
  },

  _cacheKey: function(domain) {
    return `report-${this._today()}-${domain}`;
  },

  _storeInCache: function(domain, nodes, roots, report, limits) {
    var serialized = JSON.stringify({
      tree: {
        nodes: nodes,
        roots: roots
      },
      limits,
      report: report
    });
    try {
      localStorage[this._cacheKey(domain)] = serialized;
    } catch(e) {
      localStorage.clear();
      localStorage[this._cacheKey(domain)] = serialized;
    }
  }

});

module.exports = ReportStore;
