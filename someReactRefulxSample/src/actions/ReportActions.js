import {createActions} from 'reflux';
import _ from 'lodash';
let debug = require('debug');

debug = debug('project:ReportActions');

const ReportActions = createActions([
  'load',
  'loadMultiple',
  'domainChange',
  'setStatMode',
  'setCalendarMonth',
  'setCalendarDate',
  'exportToXls',
  'exportCancel',
  'exportRedirect',
  'toggleDomain',
  'loadSnapshot',
  'setCalendarSnapshot',
  'setSnapshotDate',
  'setSnapshotDay',
  'setSnapshotWeek',
  'setSnapshotMonth'
]);

export default ReportActions;

if (process.env.NODE_ENV !== 'production') {
  _.forOwn(ReportActions, function(action, name) {
    action.preEmit = function(...args) {
      args.unshift(name);
      debug.apply(null, args);
    };
  });
}


ReportActions.domainChange.listen(function(domain) {
  window.scrollTo(0, 0);
});
