var debounce = require('debounce');


module.exports = {

  getInitialState: function() {
    return {
      viewportWidth: 0,
      viewportHeight: 0
    };
  },

  __onViewportResize: debounce(function() {
    this.setState({
      viewportWidth: window.innerWidth,
      viewportHeight: window.innerHeight
    });
  }, 100),

  componentDidMount: function() {
    window.addEventListener('resize', this.__onViewportResize, false);
    this.__onViewportResize();
  },

  componentWillUnmount: function() {
    window.removeEventListener('resize', this.__onViewportResize);
  }

};
