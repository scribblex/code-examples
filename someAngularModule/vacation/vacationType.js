(function (angular, _) {
    'use strict';

    angular
        .module('vacation')
        .filter('vacationType', vacationType);

    function vacationType () {
        return function (id, catalog) {
            if (id && catalog) {
                var target = _.findWhere(catalog, {TipOtpuskaId: id});

                return target ? target.DisplayName : '';
            }
        }
    }
})(angular, _);
