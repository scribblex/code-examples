(function (angular) {
    'use strict';

    angular
        .module('vacation')
        .directive('validateOrderNumber', validateOrderNumber);

    validateOrderNumber.$inject = ['$http'];

    function validateOrderNumber ($http) {
        return {
            require: 'ngModel',
            link: linkFunc
        };

        function linkFunc (scope, elem, attr, ngModelCtrl) {
            function orderChecker (viewValue) {
                ngModelCtrl.$setValidity('checkOrder', true);

                if (scope.vac.currentVacation && viewValue) {
                    var beginDate = scope.vac.currentVacation.DateBegin;
                    var orderDate = scope.vac.currentVacation.OrderDate;
                    var orderNumber = viewValue;
                    var id = scope.vac.currentVacation.Id;
                    $http.post('/Otpusk/ValidateOrderNumber', {Id: id, orderNumber: orderNumber, orderDate: orderDate,
                        dateBegin: beginDate}).success(function (response) {
                        if (response.Status == 0) {
                            ngModelCtrl.$setValidity('checkOrder', true);
                            ngModelCtrl.$error.showError = false
                        } else {
                            ngModelCtrl.$setValidity('checkOrder', false);
                            ngModelCtrl.$error.showError = 'checkOrder';
                        }
                    });
                }

                return viewValue;
            }

            ngModelCtrl.$parsers.unshift(orderChecker);
            ngModelCtrl.$formatters.unshift(orderChecker);
        }
    }
})(angular);
