(function (angular) {
    'use strict';

    angular
        .module('vacation')
        .filter('vacationLength', vacationLength);

    vacationLength.$inject = ['$filter'];

    function vacationLength ($filter) {
        return function (countData) {
            var otpuskDays;
            var holidayDays;
            var output = '';

            if (countData) {
                otpuskDays = countData.OtpuskDays;
                holidayDays = countData.HolidayDays;
                output = otpuskDays > 0 ? $filter('declension')(otpuskDays, 'день', 'дня', 'дней') : '';

                if (otpuskDays > 0 && holidayDays > 0) {
                    output += ', ' + $filter('declension')(holidayDays, 'праздничный', 'праздничных', 'праздничных');
                } else if (otpuskDays === 0 && holidayDays > 0) {
                    output += $filter('declension')(holidayDays, 'праздничный', 'праздничных', 'праздничных');
                }
            }

            return output;
        };
    }
})(angular);
