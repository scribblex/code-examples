(function (angular, _, moment) {
    'use strict';

    angular
        .module('vacation')
        .controller('VacationCtrl', vacationCtrl);

    vacationCtrl.$inject = ['$scope', '$routeParams', '$RangesService', 'docDownload', '$interval',
        '$filter', 'employeeListForPopup', 'CompanyData', 'employeePageEvents', 'vacationApiSvc', '$http', '$q'];

    function vacationCtrl ($scope, $routeParams, RangesService, docDownload, $interval, vacationApiSvc,
                           $filter, employeeListForPopup, CompanyData, employeePageEvents, $http, $q) {

        RangesService.flush('datepicker_vac.currentVacation.OrderDate');
        RangesService.flush('datepicker_vac.currentVacation.DateBegin');
        RangesService.flush('datepicker_vac.currentVacation.DateEnd');
        RangesService.flush('datepicker_vac.currentVacation.NachislenieDate');
        RangesService.flush('backend');

        $scope.mainSpinner = true;
        var employeeId = $routeParams.EmployeeId;
        var vacationId = $routeParams.VacationId;
        var preselectdVacation;
        var context = this;
        var createRange = RangesService.createDependentRange;
        var BeginDatepicker = 'vac.currentVacation.DateBegin';
        var currentDateEndTo;
        var currentDateEndFrom;
        var currentDateOrderFrom;
        var currentNachislenieDateBegin;
        var currentNachislenieDateEnd;
        var employeeInfo = _.findWhere(employeeListForPopup, {id: employeeId});
        var contractId = employeeInfo.contractId;
        var employeeContractStartDate;

        context.employeeName = employeeInfo.name;
        context.sex = employeeInfo.sex;
        context.isMain = employeeInfo.isMain;
        context.showTip = true;

        context.save = function (disable) {
            if (disable || context.calculationInProcess) {
                return false;
            }

            context.saving = true;
            context.currentVacation.sortId = dateToNum(context.currentVacation.DateBegin);
            var saver = angular.copy(context.currentVacation);
            saver.TypeCatalogId = saver.Type.TipOtpuskaId;
            if (saver.Type.Alias === 'Drugoe') {
                saver.NachislenieDate = saver.DateBegin;
            }
            var config = {
                method: 'POST',
                url: '/Otpusk/OtpuskPopupSave',
                data: saver
            };
            var dfd = $q.defer();

            dfd.promise.then(function (response) {
                context.uselessCalculationBreaker = true;
                var edition = _.findWhere(context.vacationCollection, {Id: response.model.Id});
                if (edition) {
                    angular.extend(edition, context.currentVacation);
                    context.selectItem(edition, true);
                } else {
                    saver.Id = response.model.Id;
                    context.vacationCollection.push(saver);
                    context.selectItem(saver, true);
                }
                ResponseAction.showConfirmMessage(response);
                context.exceludePeriods = response.model.ExcludedPeriods;
                context.newVacation = response.model.NewOtpusk;
                context.unusedDays = response.model.UnusedDaysForToday;
                parseExcludedPeriods(context.exceludePeriods);

                if (response.model.Badge.IsBadgeOperation) {
                    badgeChange(response.model.Badge.BadgeId);
                }
                var recount = _.filter(context.vacationCollection, function (item) {
                    return dateToNum(item.DateBegin) > dateToNum(context.selectedVacation.DateBegin)
                });

                recount = _.map(recount, function (item) {
                    return item.Id;
                });

                if (recount.length > 0) {
                    $http.post('/Otpusk/OtpuskPopupGetCalculationList', {
                        employeeId: employeeId,
                        otpuskaIds: recount}).success(function (response) {
                        _.each(response, function (item) {
                            var vacation = _.findWhere(context.vacationCollection, {Id: item.Id});

                            if (vacation.Calculation !== null) {
                                vacation.Calculation = item.Calculation;
                            }
                        });
                    });
                }

                if (contractId) {
                    employeePageEvents.eventsColumnUpdate(employeeId, contractId);
                }

                context.saving = false;
                context.edition = false;
            }, function () {
                context.saving = false;
            });

            context.justSelected = false;
            context.justSelectedEnd = false;

            return {
                config: config,
                deferred: dfd
            };
        };

        context.selectItem = function (item, calm) {
            _.each(context.vacationCollection, function (itm) {
                itm.selected = false;
            });
            if (item) {
                if (item.Calculation === null && !calm) {
                    $scope.miniSpinner = true;
                    $http.post('/Otpusk/OtpuskPopupGetNeighbourCalculation', {employeeId: employeeId,
                        otpuskId: item.Id})
                        .success(function (response) {
                            _.each(response, function (item) {
                                var vacation = _.findWhere(context.vacationCollection, {Id: item.Id});

                                if (vacation.Calculation === null) {
                                    vacation.Calculation = item.Calculation;
                                }
                            });
                            $scope.miniSpinner = false;
                        });
                }
                var positionStack;
                var position;
                item.selected = true;
                context.selectedVacation = item;
                context.edition = false;

                positionStack = _.filter(context.positions, function (itm) {
                    return dateToNum(itm.Date) <= dateToNum(item.OrderDate);
                });
                if (positionStack.length === 1) {
                    position = positionStack[0].Title;
                } else {
                    position = _.max(positionStack, function (itm) {
                        return dateToNum(itm.Date);
                    }).Title;
                }
                context.selectedVacation.position = position;
            } else {
                context.justSelected = false;
                context.justSelectedEnd = false;
                context.currentVacation = angular.copy(context.newVacation);
                context.currentVacation.Type = context.vacationCatalog[0];
                context.edition = true;
                context.selectedVacation = null;
                var editionPeriods = angular.copy(context.exceludePeriods);
                parseExcludedPeriods(editionPeriods);
            }
        };

        context.edit = function () {
            context.justSelected = false;
            context.justSelectedEnd = false;
            context.currentVacation = angular.copy(context.selectedVacation);
            context.currentVacation.Type =
                _.findWhere(context.vacationCatalog, {TipOtpuskaId: context.currentVacation.TypeCatalogId});
            context.edition = true;
            var editionPeriods = angular.copy(context.exceludePeriods);
            var uselessPeriod = _.findWhere(editionPeriods, {EntityId: context.currentVacation.Id});
            var uselessIndex = editionPeriods.indexOf(uselessPeriod);
            editionPeriods.splice(uselessIndex, 1);
            parseExcludedPeriods(editionPeriods);
        };

        context.cancelEdit = function () {
            if (context.selectedVacation) {
                context.edition = false;
            } else {
                $scope.popup.popupCloserService();
            }
        };

        context.remove = function (id) {
            if (confirm('Удалить отпуск?') === true) {
                var recount = _.filter(context.vacationCollection, function (item) {
                    return dateToNum(item.DateBegin) > dateToNum(context.selectedVacation.DateBegin);
                });

                recount = _.map(recount, function (item) {
                    return item.Id;
                });

                var selectNext = _.find(context.vacationCollection, function (item) {
                    return dateToNum(context.selectedVacation.DateBegin) > dateToNum(item.DateBegin);
                });

                context.deleting = true;
                var config = {
                    method: 'POST',
                    url: '/Otpusk/OtpuskPopupDelete',
                    data: {id: id}
                };
                var dfd = $q.defer();

                dfd.promise.then(function (response) {
                    ResponseAction.showConfirmMessage(response);
                    context.exceludePeriods = response.model.ExcludedPeriods;
                    context.newVacation = response.model.NewOtpusk;
                    context.unusedDays = response.model.UnusedDaysForToday;
                    parseExcludedPeriods(context.exceludePeriods);
                    var deleted = _.findWhere(context.vacationCollection, {Id: id});
                    var index = context.vacationCollection.indexOf(deleted);
                    context.vacationCollection.splice(index, 1);
                    if (!selectNext) {
                        selectNext = _.min(context.vacationCollection, function (item) {
                            return dateToNum(item.DateBegin);
                        });
                    }
                    if (selectNext === Infinity) {
                        selectNext = null;
                    }
                    context.selectItem(selectNext);
                    context.deleting = false;

                    if (response.model.Badge.IsBadgeOperation) {
                        badgeChange(response.model.Badge.BadgeId);
                    }

                    if (recount.length > 0) {
                        $http.post('/Otpusk/OtpuskPopupGetCalculationList', {
                            employeeId: employeeId, otpuskaIds: recount})
                            .success(function (response) {
                                _.each(response, function (item) {
                                    var vacation = _.findWhere(context.vacationCollection, {Id: item.Id});
                                    if (vacation.Calculation !== null) {
                                        vacation.Calculation = item.Calculation;
                                    }
                                });
                            });
                    }
                    if (contractId) {
                        employeePageEvents.eventsColumnUpdate(employeeId, contractId);
                    }
                }, function () {
                    context.deleting = false;
                });

                return {
                    config: config,
                    deferred: dfd
                };
            }
        };

        context.closePop = function () {
            context.selectItem(null);
            $scope.popup.popupCloserService();
        };

        context.hideTip = function () {
            context.showTip = false;
        };

        function parseExcludedPeriods (periods) {
            RangesService.flush('backend');

            _.each(periods, function (period) {
                var range = RangesService.createBackendRange(period);
                var reasonType = Number(period.ReasonType);
                if (reasonType === 6 || reasonType === 7 || reasonType === 8 || reasonType === 9 ||
                    reasonType === 10) {
                    range.expect('vac.currentVacation.OrderDate');
                    range.expect('vac.currentVacation.NachislenieDate');
                } else if (reasonType === 12) {
                    range.expect('vac.currentVacation.NachislenieDate');
                }
            });
        }

        function reCalculate () {
            context.calculationInProcess = true;
            $http.post('/Otpusk/OtpuskPopupGetCalculation', context.currentVacation).success(function (response) {
                context.currentVacation.Calculation = response;
                context.calculationInProcess = false;
                context.showTip = true;
            })
        }

        function dateToNum (date) {
            var reversedDate = moment(date, 'D.mm.YYYY').format('YYYYmmDD');

            return Number(reversedDate);
        }

        function init (response) {
            context.vacationCatalog = response.TipyOtpuskovCatalog;
            context.newVacation = response.NewOtpusk;
            context.vacationCollection = response.OtpuskaList;
            context.exceludePeriods = response.ExcludedPeriods;
            context.unusedDays = response.UnusedDaysForToday;
            context.positions = response.Dolghosti;

            employeeContractStartDate = _.findWhere(context.exceludePeriods, {ReasonType: 0}).DateEnd;

            parseExcludedPeriods(context.exceludePeriods);

            context.currentVacation = angular.copy(response.NewOtpusk);
            context.currentVacation.Type = context.vacationCatalog[0];

            _.each(context.vacationCollection, function (item) {
                item.sortId = dateToNum(item.DateBegin);
            });

            $scope.isLoaded = true;
            $scope.mainSpinner = false;
            context.edition = true;
            currentDateEndTo = createRange('vac.currentVacation.DateEnd', 'Не ранее даты начала отпуска');
            currentDateEndFrom = createRange('vac.currentVacation.DateEnd', 'Заблокировано из-за наличия отклонений');
            currentDateOrderFrom = createRange('vac.currentVacation.OrderDate', 'Не позднее начальной даты');
            currentNachislenieDateBegin = createRange('vac.currentVacation.NachislenieDate',
                'Не ранее, чем за месяц до начала отпуска');
            currentNachislenieDateEnd = createRange('vac.currentVacation.NachislenieDate',
                'Не позднее, чем за три дня до начала отпуска');
        }

        function badgeChange (id) {
            if (id) {
                var source = _.findWhere(context.vacationCollection, {Id: id});
                var endDate = $filter('bolDate')(source.DateEnd, 'd MMMM');
            }
            employeePageEvents.vacationBadgeInsert(employeeId, id, endDate, 'vacation', 'Employee.InfoStatus.Vacation');
        }

        var lazyCount = _.debounce(reCalculate, 200);

        $scope.$watch('vac.currentVacation.DateBegin', function (newValue, oldValue) {
            if (context.uselessCalculationBreaker) {
                context.uselessCalculationBreaker = false;
            } else {
                if (context.currentVacation && newValue && oldValue) {
                    var dateEndActiveRange = RangesService.getActiveRangeForDatepicker(BeginDatepicker,
                        context.currentVacation.DateBegin);
                    var dateFrom = moment(context.currentVacation.DateBegin, 'D.M.YYYY');
                    if (currentDateEndTo) {
                        currentDateEndTo.to = dateFrom.clone().subtract('day', 1);
                    }
                    if (currentDateEndFrom) {
                        currentDateEndFrom.from = dateEndActiveRange.to;
                    }
                    if (currentDateOrderFrom) {
                        currentDateOrderFrom.from = dateFrom.clone().add('day', 1);
                    }
                    if (currentNachislenieDateEnd) {
                        currentNachislenieDateEnd.from = dateFrom.clone().add('day', 1);
                    }
                    if (currentNachislenieDateBegin) {
                        currentNachislenieDateBegin.to =
                            moment(employeeContractStartDate, 'D.M.YYYY') > dateFrom.clone().subtract('month', 1) ?
                                moment(employeeContractStartDate, 'D.M.YYYY') : dateFrom.clone().subtract('month', 1);
                    }

                    if (dateToNum(newValue) !== dateToNum(oldValue) && context.justSelected) {
                        lazyCount();
                    }

                    if (!context.justSelected) {
                        context.justSelected = true;
                    }
                }
            }
        });

        $scope.$watch('vac.currentVacation.DateEnd', function (newValue, oldValue) {
            if (context.uselessCalculationBreaker) {
                context.uselessCalculationBreaker = false;
            } else {
                if (context.currentVacation && newValue && oldValue && !context.saving) {
                    if (dateToNum(newValue) !== dateToNum(oldValue) && context.justSelectedEnd) {
                        lazyCount();
                    }

                    if (!context.justSelectedEnd) {
                        context.justSelectedEnd = true;
                    }
                }
            }
        });

        context.typeChanger = function () {
            context.currentVacation.TypeCatalogId = context.currentVacation.Type.TipOtpuskaId;
            lazyCount();
        };

        context.downloadDocs = function (type, format, docCount) {
            var downloadId = newguid();
            var downloadIds = [newguid(), newguid()];
            var coockieName = 'downloadIdList' + downloadId;
            var downloadCount = docCount || 1;
            var btnPreloadTimeLimit = 120000;
            var timeStep = 200;
            var intervalId;
            var company;
            var method;
            var url;

            if (type === 'Order') {
                if (context.orderDownload) {
                    return false;
                }
                context.orderDownload = true;
                company = '';
                method = 'GetOtpuskReport';
                url =  '/Otpusk/' + method + '/?otpuskId=' + context.selectedVacation.Id +
                '&isAngular=true&format=' + format + company + '&downloadId=' + downloadId;
                docDownload.getDoc(url, 0, 12000);

                intervalId = $interval(function () {
                    if (btnPreloadTimeLimit <= 0 ||
                        ($.cookie(coockieName) && $.cookie(coockieName).split(',').length === downloadCount)) {
                        $interval.cancel(intervalId);
                        $.cookie(coockieName, null, {path: '/', expires: -1});
                        context.orderDownload = false;
                        context.t60Download = false;
                        context.everyDownload = false;
                    }
                    btnPreloadTimeLimit -= timeStep;
                }, 200);
            } else if (type === 'T60') {
                if (context.t60Download) {
                    return false;
                }
                context.t60Download = true;
                company = '&companyId=' + CompanyData.id + '&employeeId=' + employeeId;
                method = 'GetT60';
                url =  '/Otpusk/' + method + '/?otpuskId=' + context.selectedVacation.Id +
                '&isAngular=true&format=' + format + company + '&downloadId=' + downloadId;
                docDownload.getDoc(url, 0, 12000);

                intervalId = $interval(function () {
                    if (btnPreloadTimeLimit <= 0 ||
                        ($.cookie(coockieName) && $.cookie(coockieName).split(',').length === downloadCount)) {
                        $interval.cancel(intervalId);
                        $.cookie(coockieName, null, {path: '/', expires: -1});
                        context.orderDownload = false;
                        context.t60Download = false;
                        context.everyDownload = false;
                    }
                    btnPreloadTimeLimit -= timeStep;
                }, 200);
            } else if (type === 'All') {
                if (context.everyDownload) {
                    return false;
                }
                context.everyDownload = true;
                var urlArr = [
                    '/Otpusk/GetOtpuskReport/?otpuskId=' + context.selectedVacation.Id +
                    '&isAngular=true&format=Pdf&downloadId=' + downloadIds[0],
                    '/Otpusk/GetT60/?otpuskId=' + context.selectedVacation.Id +
                    '&isAngular=true&format=Pdf&companyId=' + CompanyData.id + '&employeeId=' +
                    employeeId + '&downloadId=' + downloadIds[1]
                ];

                _.each(_.range(downloadCount), function (i) {
                    docDownload.getDoc(urlArr[i], 0, 12000);
                });

                intervalId = $interval(function () {
                    _.each(_.range(downloadCount), function (i) {
                        if (btnPreloadTimeLimit <= 0 ||
                            $.cookie('downloadIdList' + downloadIds[i])) {
                            $interval.cancel(intervalId);
                            $.cookie('downloadIdList' + downloadIds[i], null, {path: '/', expires: -1});
                            context.orderDownload = false;
                            context.t60Download = false;
                            context.everyDownload = false;
                        }
                        btnPreloadTimeLimit -= timeStep;
                    });
                }, 200);
            }
        };

        context.isUnpaybleVacation = function (id) {
            var catItem = _.findWhere(context.vacationCatalog, {TipOtpuskaId: id});
            return catItem ? catItem.Alias === 'Drugoe' : false;
        };

        if (vacationId) {
            vacationApiSvc.getNeighbourVacation({/**/}).success(function (response) {
                init(response);
                preselectdVacation = _.findWhere(context.vacationCollection, {Id: vacationId});
                context.selectItem(preselectdVacation);
            });
        } else {
            vacationApiSvc.getDefaultVacation({employeeId: employeeId}).success(function (response) {
                init(response);
            });
        }

        if (employeeId) {
            $scope.popup.popupOpenerService();
        }
    }
})(angular, _, moment);
