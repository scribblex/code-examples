(function (angular) {
    'use strict';

    angular
            .module('vacation', [])
            .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.
                    when('/vacation/:EmployeeId', {templateUrl: './partials/addVacation.html',
                            controller: 'VacationCtrl', controllerAs: 'vac'}).
                    when('/vacation/:EmployeeId/:VacationId',
                        {templateUrl: './partials/addVacation.html',
                        controller: 'VacationCtrl', controllerAs: 'vac'});
            }]);
})(angular);
