(function (angular) {
    'use strict';

    angular
        .module('dismissal')
        .factory('vacationApiSvc', vacationApiSvc);

    vacationApiSvc.$inject = ['$http', 'periodReopenSvc'];

    function vacationApiSvc ($http, $window, periodReopenSvc) {
        return {
            getTerminationDefaults: getTerminationDefaults,
            getTerminationDetails: getTerminationDetails,
            getNeighbourVacation: getNeighbourVacation,
            getDefaultVacation: getDefaultVacation
        };

        function getDefaultVacation (obj) {
            return $http.post('/Otpusk/OtpuskPopupNew', obj);
        };

        function getNeighbourVacation (obj) {
            return $http.post('/Otpusk/OtpuskPopupNeighbour', obj);
        };

        function getTerminationDefaults (contractId) {
            return $http.post('/Contract/ContractTerminate', {contractId: contractId})
                .then(function (response) {
                    return response;
                });
        }

        function getTerminationDetails (contractId, dateEnd) {
            var requestObject = {
                contractId: contractId,
                dateEnd: dateEnd
            };

            return $http.post('/Contract/TerminateGetCalcKompensatsiya', requestObject)
                .then(function (response) {
                    return response;
                });
        }
    }
})(angular);
