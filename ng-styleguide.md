#Angular.js styleguide

##Основные принципы

###Один файл соответствуе одному компоненту

###Замыкания 

```javascript

    (function(angular) {
        'use strict';
        angular
            .module('app')
            .factory('logger', logger);
     
        function logger() { }
    })(angular);

```

###Принцип Lift

```javascript

    (function(angular) {
        'use strict';
        angular
            .module('app')
            .directive('dir', dir);
     
        function dir() {
            return {
                link: linkFunc
            };
            
            function linkFunc () {}
        }
    })(angular);

```

###Передача зависимостей

```javascript

    (function(angular) {
        'use strict';
        angular
            .module('app')
            .factory('logger', logger);
     
        logger.$inject = ['$http', '$q'];
        function logger($http, $q) { }
    })(angular);

```

### Промисы для Route Resolve.
Почему? Контроллеру могут быть нужны данные перед инициализацией. Эти данные могут приходить
промисом от фабрики или $http. Использование route resolve позволяет разрешать [resolve] промис перед тем,
как запускается логика контроллера, поэтому контроллер может опираться на данные из промиса.

```javascript

    (function(angular) {
        'use strict';
        
        // route-config.js
        angular
           .module('app')
           .config(config);
         
        function config($routeProvider) {
           $routeProvider
               .when('/avengers', {
                   templateUrl: 'avengers.html',
                   controller: 'Avengers',
                   controllerAs: 'vm',
                   resolve: {
                       moviesPrepService: function(movieService) {
                           return movieService.getMovies();
                       }
                   }
               });
        }
         
        // avengers.js
        angular
           .module('app')
           .controller('Avengers', Avengers);
         
        Avengers.$inject = ['moviesPrepService'];
        function Avengers(moviesPrepService) {
             var vm = this;
             vm.movies = moviesPrepService.movies;
        }
    })(angular);

```
### Избегайте обращаться к серверу внутри контроллера! Для этого существуют фабрики!
