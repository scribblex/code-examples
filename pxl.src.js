function CookieStorage(origin, path) {
    // some code
}

CookieStorage.prototype = {
    constructor: CookieStorage,

    sync: function(key, callback) {
        var that = this,
            handleIframeMessage = function(event) {
                that.handleMessage(event);
            };

        if (!this._iframe && this.supported) {
            this._iframe = globals.document.createElement('iframe');
            this._iframe.style.cssText = 'position:absolute;width:0px;height:0px;left:-9999px;border:none;top:0;';
            globals.document.body.appendChild(this._iframe);

            Utils.bind(this._iframe, 'load', that.iframeLoadedHandler, that);
            Utils.bind(globals, 'message', handleIframeMessage);

            this._iframe.src = this._origin + this._path;

            var request = { id: ++this._id, type: 'sync', key: key },
                data    = { request: request, callback: callback };
            if (this._iframeReady) {
                this.sendRequest(data);
            } else {
                this._queue.push(data);
            }

            this._timeout = setTimeout(function() {
                callback();
                Utils.unbind(globals, 'message', handleIframeMessage);
            }, IFRAME_TIMEOUT * 1000);
        }
    },

    sendRequest: function(data) {
        if (this._iframe) {
            try {
                this._requests[data.request.id] = data;
                this._iframe.contentWindow.postMessage(JSON.stringify(data.request), this._origin);
            } catch(e) {
                Utils.warn('Iframe is not available.');
            }
        }
    },

    iframeLoadedHandler: function() {
        var i, len;
        this._iframeReady = true;

        if (this._queue.length) {
            for (i = 0, len = this._queue.length; i < len; i++) {
                this.sendRequest(this._queue[i]);
            }
            this._queue = [];
        }
    },

    handleMessage: function(event) {
        var data;
        if (event.origin === this._origin) {
            clearTimeout(this._timeout);
            try {
                data = JSON.parse(event.data);
                if (this._requests[data.id]) {
                    if (typeof this._requests[data.id].callback === 'function') {
                        this._requests[data.id].callback(data.value);
                    }
                    delete this._requests[data.id];
                }
            } catch (e) {
                Utils.warn('Response is invalid.');
            }
        }
    }
}
