function shownPixelsAnalizer (ids, cookieName) {
    if (arguments.callee._singletonInstance) {
        return arguments.callee._singletonInstance;
    }

    arguments.callee._singletonInstance = this;

    return init();

    /**
     * @function getCookie
     * @param {string} name
     * @returns {*}
     */
    function getCookie (name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : 0;
    }

    /**
     * @function setCookie
     * @param {string} name
     * @param {string} value
     * @param {object} props
     */
    function setCookie (name, value, props) {
        props = props || {};

        var exp = props.expires;
        if (typeof exp === "number" && exp) {
            var d = new Date();
            d.setTime(d.getTime() + exp*1000);
            exp = props.expires = d;
        }

        if (exp && exp.toUTCString) {
            props.expires = exp.toUTCString();
        }

        value = encodeURIComponent(value);
        var updatedCookie = name + "=" + value;

        for (var propName in props) {
            updatedCookie += "; " + propName;
            var propValue = props[propName];

            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
    }

    /**
     * @function cookieCheck
     * @param {number} bitMask
     * @param {number} pixelId
     * @returns {boolean}
     */
    function cookieCheck (bitMask, pixelId) {
        if (bitMask && pixelId) {
            return bitMask & (1 << pixelId);
        }

        return false;
    }

    function setInMask (bitMask, pixelId) {
        return bitMask |= 1 << pixelId;
    }

    function init () {
        var cookieValue;
        var img;

        for (var i = 0; i < ids.length; i++) {
            cookieValue = getCookie(cookieName);

            if (!cookieCheck(cookieValue, i)) {
                var time = (new Date()).getMilliseconds() + 10800; //10800 -> 3h

                setCookie(cookieName, setInMask(cookieValue, i),  {expires: time});
                img = new Image();
                img.src = ids[i];
            }
        }
    }
}
