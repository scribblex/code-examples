CSS Style Guide
========

Полезные и рекомендуемые для ознакомления ссылки:

* [Принципы написания однородного CSS-кода](https://github.com/necolas/idiomatic-css/tree/master/translations/ru-RU)
* [БЭМ определения](http://ru.bem.info/method/definitions/)
* [Use efficient CSS selectors — Google Developers](https://developers.google.com/speed/docs/best-practices/rendering#UseEfficientCSSSelectors)
* [Writing efficient CSS - Mozilla Developer Network](https://developer.mozilla.org/en-US/docs/CSS/Writing_Efficient_CSS)
* [Stylesheet Limits in Internet Explorer - IEInternals MSDN Blogs](http://blogs.msdn.com/b/ieinternals/archive/2011/05/14/10164546.aspx)
* [Can I use...](http://caniuse.com/)
* [Common fonts to all versions of Windows & Mac equivalents](http://www.ampsoft.net/webdesign-l/WindowsMacFonts.html)
* [Архитектура CSS](http://web-standards.ru/articles/css-architecture/)


## Action БЭМ

### Блок

Блок - это независимый фрагмент страницы, который можно целиком описать своими стилями.

Для названия блока используются строчные буквы с "-" в качестве разделителя слов.

Он никак не зависит от контекста, в котором он находится.

```css
.block-name {}
```

### Элемент

У блоков могут быть элементы. Элементы – это фрагменты, которые неактуальны вне блока.

Для обозначения элемента используется название блока-родителя и через двойное подчеркивание название самого блока.

Не может быть элементов второго уровня.

Вложенность элементов нежелательна (возможно, ее можно заменить на отдельный блок-элемент).

```css
.block-name__element-name {}
```

Хорошо:

```html
<div class="block-name">
    <h2 class="block-name__title"></h2>
    <div class="block-name__text"></div>
</div>
```

Плохо:

```html
<div class="block-name">
    <div class="block-name__element">
        <span class="block-name__element__text"></span>
    </div>
</div>
```

### Модификатор

Так же у блоков и элементов есть модификаторы. Они используются для изменения части свойств блока/элемента.

Например, в некоторых случаях нужно оставить все как есть, но поменять фон.

Для обозначения модификатора используется название блока/элемента и название модификатора через одинарное подчеркивание.

Желательно отобразить в названии модификатора, что он изменяет и как.

```css
.block-name_big {}
```

```html
<div class="block-name block-name_big"></div>
```

Для модификаторов также можно использовать одно подчеркивание для отделение имени модификатора от имени блока и еще одно подчеркивание для отделения значения модификатора от его имени.

```css
.menu_size_big {
  /* CSS code to specify big height */
}

.menu_size_small {
  /* CSS code to specify small height */
}

.menu_type_buttons .menu__item {
  /* CSS code to change item's look */
}
```

В таком виде с модификаторами будет удобно работать в JS'e, например:

```js
$('.menu').bemMod('size', 'big');
```

Где bemMod может быть хэлпером, который будет менять модификатор блока с именем size на нужное значение (small или big)


### Общие рекомендации

* При верстке блоками не используются в селекторах теги (названия элементов) – исключение user-generated контент от wysiwyg подобных редакторов.
* Вся страница должна состоять только из блоков, не допускаются элементы вне блоков.
* Не допускается смешение классов от разных блоков/элементов.


## Верстка блока

* При разработке блока сначала пытаемся понять, есть ли уже такой блок и используем его (или добавляем модификатор при необходимости).
* Используем переменные и миксины из variables.less и mixins.less. Если встречаются нестандратные отступы/цвета обсуждаем их необходимость с дизайнерами.
